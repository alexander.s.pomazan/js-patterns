class Calculator {
	value = 0

	calculate(operator, operand) {
		switch(operator) {
			case '+': 
				this.value += operand; 
				break;
	    case '-': 
				this.value -= operand; 
				break;
			default:
				alert("Неизвестный оператор");
				break;
		}
		console.log(this.value)
	}
}


class CalculatorCommand {
	calculator = null
	operator = null
	operand = null

	execute(newCalculator, newOperator, newOperand) {
		if (
			newCalculator instanceof Calculator
			&& typeof newOperator === 'string'
			&& typeof newOperand === 'number'
		) {
			this.calculator = newCalculator
			this.operator = newOperator
			this.operand = newOperand
		}
		this.calculator.calculate(this.operator, this.operand)
	}

	unExecute() {
		this.calculator.calculate(this.undo(this.operator), this.operand)
	}

	undo(operator) {
		switch(operator) {
			case '+': return '-'
			case '-': return '+'
			default: return ' '
		}
	}
}

class Invoker {
	calculator = new Calculator()
	commands = []
	current = 0

	compute(operator, operand) {
		const newCommand = new CalculatorCommand()

		if (this.current < this.commands.length < 1) {
			this.commands.splice(this.current)
		}
		newCommand.execute(this.calculator, operator, operand)
		this.commands.push(newCommand)
		this.current++
	}

	undo(levels) {
		for (let i = 0; i < levels; i++) {
			if (this.current > 0) {
				this.commands[--this.current].unExecute();
			}
		}
	}

	redo(levels) {
		for (let i = 0; i < levels; i++) {
			if (this.current < this.commands.length) {
				this.commands[current++].execute();
			}
		}
	}
}

const invoker = new Invoker()

invoker.compute("+", 2)
invoker.compute("+", 3)
invoker.compute("-", 1)
invoker.compute("+", 6)
invoker.undo(3)