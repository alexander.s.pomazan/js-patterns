class ObserverList {
	constructor () {
		this.observerList = []
	}

	add(observer) {
		this.observerList = [...this.observerList, observer]
		return this.observerList
	}

	count() {
		return this.observerList.length
	}

	get(index) {
		return this.observerList[index]
	}

	indexOf(obj, startIndex = 0) {
		return this.observerList.indexOf(obj, startIndex)
	}

	removeAt(index) {
		this.observerList = this.observerList.filter((_, idx) => idx !== index)
		return this.observerList
	}

}

class Subject {
	constructor() {
		this.observers = new ObserverList()
	}

	addObserver(observer) {
		this.observers.add(observer)
	}

	removeObserver(observer) {
		this.observers.remoteAt(this.observers.indexOf(observer, 0))
	}

	notify(context){
		const observerCount = this.observers.count()

		for (let i = 0; i < observerCount; i++) {
			this.observers.get(i).update(context)
		}
	}
}

class Observer {
	constructor (update) {
		this.update = update
	}
}


// Concrete Subject
function extend( obj, extension ){
  for ( var key in extension ){
    obj[key] = extension[key];
  }
}

const container = document.getElementById('observersContainer')

const controlCheckbox = document.getElementById('mainCheckbox')
controlCheckbox.subject = new Subject()

controlCheckbox.addEventListener(
	'click',
	() => controlCheckbox.subject.notify(controlCheckbox.checked),
	false
)



const addBtn = document.getElementById('addNewObserver')

const addNewObserver = () => {
	const update = function(value) {
		this.checked = value
	}

	// Concrete Observer
	const check = document.createElement('input') 
	extend(check, new Observer(update))
	check.type = 'checkbox'

	controlCheckbox.subject.addObserver(check)
	container.appendChild(check)
}

addBtn.addEventListener('click', addNewObserver)

