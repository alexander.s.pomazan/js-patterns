class PubSub {
	constructor() {
		this.topics = {}
		this.subUid = -1
	}

	generateSubUid() {
		const uid = this.subUid.toString()
		this.subUid = this.subUid + 1

		return uid
	}

	subscribe(topic, func) {
		const subscriberToken = this.generateSubUid()

		const newSubscriber = {
			token: subscriberToken,
			func
		}

		if (!this.topics[topic]) {
			this.topics = {
				...this.topics,
				[topic]: [newSubscriber]
			}
		} else {
			this.topics = {
				...this.topics,
				[topic]: [...this.topics[topic], newSubscriber]
			}
		}

		return subscriberToken
	}

	unsubscribe(token) {
		this.topics = Object.entries(this.topics).reduce((topics, [topicName, topicArr]) => {
			return {
				...topics,
				[topicName]: topicArr.filter((topic) => topic.token !== token)
			}
		}, {})
	}

	publish(topic, args) {
		if (!this.topics[topic]) return false

		const subscribers = this.topics[topic]

		subscribers.forEach((sub) => sub.func(topic, args))

		return this
	}
}

const logMessage = (topics, data) => console.log( "Logging: " + topics + ": " + data );

const pubsub = new PubSub()

const subToken = pubsub.subscribe('inbox/message', logMessage)

pubsub.publish( "inbox/message", 'hello');

pubsub.unsubscribe(subToken)


